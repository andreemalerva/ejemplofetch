let paginaActual = 1;

reset();
document.getElementById('buscarElemento').addEventListener('click', function () {
    valorDelInput = document.getElementById('itemBuscar').value;
    buscar(valorDelInput)
})

document.getElementById('todos').addEventListener('click', function () {
    todos();
    document.getElementById('botonesPaginacion').style.display = 'flex';
})

document.getElementById('reset').addEventListener('click', function () {
    reset();
})

document.getElementById('siguiente').addEventListener('click', function () {
    avanzarPagina();
})

document.getElementById('atras').addEventListener('click', function () {
    retrocederPagina();
})

function reset() {
    document.getElementById('todoslosdatos').innerHTML = '';
    document.getElementById('botonesPaginacion').style.display = 'none';
    document.getElementById('itemBuscar').value = '';
}

function todos() {
    fetch('https://rickandmortyapi.com/api/character')
        .then(response => response.json())
        .then(function (data) {
            console.log(data);
            document.getElementById('todoslosdatos').innerHTML = data.results.map(datos =>
                `<div class="card">
                            <img src="${datos.image}" class="card-img-top imagenPersona" alt="">
                            <div class="card-body">
                                <h5 class="card-title nombre">${datos.name}</h5>
                                <div class="row">
                                    <div class="col"><p class="card-text especie">${datos.species}</p></div>
                                    <div class="col"><p class="card-text genero">${datos.gender}</p></div>
                                    <div class="col"><p class="card-text idCard">${datos.id}</p></div>
                                </div>
                            </div>
                        </div>`
            ).join('');
        });
}

function buscar(valorDelInput) {
    if (valorDelInput != '') {
        fetch('https://rickandmortyapi.com/api/character/' + valorDelInput)
            .then(response => response.json())
            .then(function (datosItem) {
                console.log(datosItem);
                document.getElementById('todoslosdatos').innerHTML =
                    `<div class="card busqueda">
                            <img src="${datosItem.image}" class="card-img-top imagenPersona" alt="">
                            <div class="card-body">
                                <h5 class="card-title nombre">${datosItem.name}</h5>
                                <div class="row">
                                    <div class="col"><p class="card-text especie">${datosItem.species}</p></div>
                                    <div class="col"><p class="card-text genero">${datosItem.gender}</p></div>
                                    <div class="col"><p class="card-text idCard">${datosItem.id}</p></div>

                                </div>
                            </div>
                        </div>`;
            });
    } else {
        document.getElementById('todoslosdatos').innerHTML =
            `<div class="card busqueda">
                            <img src="assets/img/cat.jpg" class="card-img-top imagenPersona" alt="no hay datos">
                            <div class="card-body">
                                <h5 class="card-title nombre">No encontramos tu busqueda</h5>
                            </div>
                        </div>`;
    }
}

function avanzarPagina() {
    paginaActual = paginaActual + 1;
    paginación(paginaActual);
}

function retrocederPagina() {
    paginaActual = paginaActual - 1;
    paginación(paginaActual);
}

function paginación(paginaNumero) {
    console.log('paginaNumero', paginaNumero)
    fetch('https://rickandmortyapi.com/api/character/?page=' + paginaNumero)
        .then(response => response.json())
        .then(function (datosPag) {
            console.log(datosPag);
            document.getElementById('todoslosdatos').innerHTML = datosPag.results.map(pageItems =>
                `<div class="card">
                            <img src="${pageItems.image}" class="card-img-top imagenPersona" alt="">
                            <div class="card-body">
                                <h5 class="card-title nombre">${pageItems.name}</h5>
                                <div class="row">
                                    <div class="col"><p class="card-text especie">${pageItems.species}</p></div>
                                    <div class="col"><p class="card-text genero">${pageItems.gender}</p></div>
                                    <div class="col"><p class="card-text idCard">${pageItems.id}</p></div>
                                </div>
                            </div>
                        </div>`
            ).join('');
        });
}

