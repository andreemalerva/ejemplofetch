<div>
    <h1>Ejemplo de uso de Fetch con JAVASCRIPT</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 andreemalerva@gmail.com
📲 2283530727
```

# Acerca del proyecto

Este proyecto esta hecho con la ayuda de tecnologías como lo son HTML, CSS, Javascript,acompañado de frameworks como *Bootstrap* y haciendo el uso de la Api de [rickandmortyapi.com](https://rickandmortyapi.com/). 

Este proyecto puede ser visualizado en: [Demo for andreemalerva | ejemplofetch](https://andreemalerva.gitlab.io/ejemplofetch)

# Politícas de privacidad

```
Las imagenes, archivos css, html y adicionales son propiedad de © 2023 LYAM ANDREE CRUZ MALERVA.
```